﻿using AutoFixture;
using FluentAssertions;
using PagarmeChallenge.Command.Api.Extensions;
using Xunit;

namespace PagarmeChallenge.Test.UnitTests.Command.Api.Extensions
{
    public class StringExtensionTests
    {
        private readonly Fixture _fixture;

        public StringExtensionTests()
            => _fixture = new Fixture();

        [Fact]
        public void ToPipedMessageShouldBeReturnStringPiped()
        {
            var words = _fixture.CreateMany<string>();
            var expectedResult = string.Join('|', words);

            var result = StringExtension.ToPipedMessage(words);
            result.Should().Be(expectedResult);
        }

        [Fact]
        public void ToPipedMessageShouldBeReturnStringEmpty()
        {
            var result = StringExtension.ToPipedMessage(null);
            result.Should().Be(string.Empty);
        }
    }
}
