﻿using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Routing;
using Moq;
using PagarmeChallenge.Command.Api.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace PagarmeChallenge.Test.UnitTests.Command.Api.Filters
{
    public class ValidationModelStateActionFilterTest
    {
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void OnActionExecutionAsyncShouldBeThrowException(bool modelStateWithError)
        {
            var actionExecutingContext = GetActionExecutingContext(modelStateWithError);
            var actionExecutionDelegate = GetActionExecutionDelegate(modelStateWithError);

            var validationModelStateActionFilter = new ValidationModelStateActionFilter();

            Func<Task> result = () => validationModelStateActionFilter
                                            .OnActionExecutionAsync(actionExecutingContext, actionExecutionDelegate);

            if (modelStateWithError)
            {
                result.Should().ThrowAsync<Exception>();
            }
            else
            {
                result.Should().NotThrowAsync<Exception>();
            }
        }

        private ActionExecutingContext GetActionExecutingContext(bool modelStateWithError)
        {
            var actionContext = GetActionContext(modelStateWithError);
            var metaData = new List<IFilterMetadata>();
            var dictinary = new Dictionary<string, object>();
            var controller = new Mock<Controller>().Object;

            return new ActionExecutingContext(
                actionContext,
                metaData,
                dictinary,
                controller);
        }

        private ActionExecutionDelegate GetActionExecutionDelegate(bool modelStateWithError)
        {
            var actionContext = GetActionContext(modelStateWithError);
            var metaData = new List<IFilterMetadata>();
            var controller = new Mock<Controller>().Object;

            return () => Task.FromResult(
                new ActionExecutedContext(actionContext, metaData, controller));
        }

        private ActionContext GetActionContext(bool modelStateWithError)
        {
            var httpContext = new DefaultHttpContext();
            var routeData = new RouteData();
            var actionDescriptior = new ActionDescriptor();
            var modelState = new ModelStateDictionary();

            if (modelStateWithError)
                modelState.AddModelError("", "error");

            return new ActionContext(
                httpContext,
                routeData,
                actionDescriptior,
                modelState);
        }
    }
}
