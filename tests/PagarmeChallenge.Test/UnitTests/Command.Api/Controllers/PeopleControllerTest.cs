﻿using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using PagarmeChallenge.Command.Api.Controllers;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.RabbitMQ.Contants;
using PagarmeChallenge.Shared.RabbitMQ.Interfaces;
using System;
using Xunit;

namespace PagarmeChallenge.Test.UnitTests.Command.Api.Controllers
{
    public class PeopleControllerTest
    {
        private readonly Fixture _fixture;
        private readonly PeopleController _personController;
        private readonly Mock<IProducerService> _producerService;

        public PeopleControllerTest()
        {
            _fixture = new Fixture();
            _producerService = new Mock<IProducerService>(MockBehavior.Strict);
            _personController = new PeopleController(_producerService.Object);
        }

        [Fact]
        public void InsertMustBeExecutedCorrectly()
        {
            var person = _fixture.Create<Person>();

            _producerService
                .Setup(x => x.Publish(QueueNamesConstants.DemographicPersonalDataQueue, person))
                .Verifiable();

            var result = (AcceptedResult)_personController.Insert(person);

            result.StatusCode.Should().Be(StatusCodes.Status202Accepted);

            _producerService.VerifyAll();
        }

        [Fact]
        public void InsertShouldFailWhenRabbitMQIsNotConnected()
        {
            var person = _fixture.Create<Person>();

            _producerService
                .Setup(x => x.Publish(QueueNamesConstants.DemographicPersonalDataQueue, person))
                .Throws<Exception>();

            var result = (BadRequestObjectResult)_personController.Insert(person);

            result.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }
    }
}
