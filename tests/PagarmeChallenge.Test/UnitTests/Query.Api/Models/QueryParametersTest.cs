﻿using AutoFixture;
using FluentAssertions;
using Moq;
using PagarmeChallenge.Query.Api.Data.Models;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using Xunit;

namespace PagarmeChallenge.Test.UnitTests.Query.Api.Models
{
    public class QueryParametersTest
    {
        [Fact]
        public void GetParametersShouldBeExecuted()
        {
            var query = new QueryParameters
            {
                Name = new Fixture().Create<string>(),
                Color = It.IsAny<Color>(),
                Region = It.IsAny<Region>(),
                EducationalLevel = It.IsAny<EducationalLevel>()
            };

            var expression = query.GetParameters();

            expression.Should().NotBeNull();
        }
    }
}
