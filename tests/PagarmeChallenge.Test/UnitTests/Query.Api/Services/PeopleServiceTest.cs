﻿using AutoFixture;
using FluentAssertions;
using Moq;
using PagarmeChallenge.Query.Api.Data.Models;
using PagarmeChallenge.Query.Api.Interfaces;
using PagarmeChallenge.Query.Api.Services;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xunit;

namespace PagarmeChallenge.Test.UnitTests.Query.Api.Services
{
    public class PeopleServiceTest
    {
        private readonly Fixture _fixture;
        private readonly Mock<IPeopleRepository> _peopleRepository;

        private IPeopleService _peopleService;

        public PeopleServiceTest()
        {
            _fixture = new Fixture();
            _peopleRepository = new Mock<IPeopleRepository>(MockBehavior.Strict);
            _peopleService = new PeopleService(_peopleRepository.Object);
        }

        [Fact]
        public async Task FilterByParametersShouldBeExecuted()
        {
            var query = _fixture.Create<QueryParameters>();
            var expectedResult = _fixture.CreateMany<Person>();

            _peopleRepository
                .Setup(x => x.FilterByParameters(It.IsAny<Expression<Func<Person,bool>>>()))
                .ReturnsAsync(expectedResult.ToList());

            var result = await _peopleService.FilterByParameters(query);

            result.Should().BeEquivalentTo(expectedResult);
        }

        [Fact]
        public async Task GetPercentagePeopleWithSameNameByRegionShouldBeExecuted()
        {
            var name = _fixture.Create<string>();
            var region = _fixture.Create<Region>();
            var expectedResult = _fixture.CreateMany<Person>();

            _peopleRepository
                .Setup(x => x.GetPeopleByRegion(region))
                .ReturnsAsync(expectedResult.ToList());

            var result = await _peopleService.GetPercentagePeopleWithSameNameByRegion(region, name);

            result.Should().Be(It.IsAny<decimal>());
        }

        [Fact]
        public void  GetPercentagePeopleWithSameNameByRegionShouldThrowException()
        {
            var name = _fixture.Create<string>();
            var region = _fixture.Create<Region>();
            var expectedResult = new List<Person>();

            _peopleRepository
                .Setup(x => x.GetPeopleByRegion(region))
                .ReturnsAsync(expectedResult);

            Func<Task> result = () => _peopleService.GetPercentagePeopleWithSameNameByRegion(region, name);

            result.Should().ThrowAsync<Exception>();
        }
    }
}
