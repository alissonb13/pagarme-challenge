﻿using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using PagarmeChallenge.Query.Api.Controllers;
using PagarmeChallenge.Query.Api.Data.Models;
using PagarmeChallenge.Query.Api.Interfaces;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using System;
using System.Threading.Tasks;
using Xunit;

namespace PagarmeChallenge.Test.UnitTests.Query.Api.Controllers
{
    public class PeopleControllerTest
    {
        private readonly Fixture _fixture;
        private readonly PeopleController _peopleController;
        private readonly Mock<IPeopleService> _peopleService;

        public PeopleControllerTest()
        {
            _fixture = new Fixture();
            _peopleService = new Mock<IPeopleService>();

            _peopleController = new PeopleController(_peopleService.Object);
        }

        [Fact]
        public async Task FilterPeopleByParametersShouldToReturnOk()
        {
            var query = _fixture.Create<QueryParameters>();
            var expectedResult = _fixture.CreateMany<Person>();

            _peopleService
                .Setup(x => x.FilterByParameters(query))
                .ReturnsAsync(expectedResult);

            var response = await _peopleController.FilterPeopleByParameters(query);

            var result = response.Result as OkObjectResult;
            result.Value.Should().Be(expectedResult);
            result.StatusCode.Should().Be(StatusCodes.Status200OK);
        }

        [Fact]
        public async Task FilterPeopleByParametersShouldBeToReturnBadRequest()
        {
            var query = _fixture.Create<QueryParameters>();

            _peopleService
                .Setup(x => x.FilterByParameters(query))
                .ThrowsAsync(new Exception());

            var response = await _peopleController.FilterPeopleByParameters(query);

            var result = response.Result as BadRequestObjectResult;
            result.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }

        [Fact]
        public async Task GetPercentagePeopleWithSameNameByRegionShouldToReturnOk()
        {
            var name = _fixture.Create<string>();
            var region = _fixture.Create<Region>();
            var expectedResult = _fixture.Create<decimal>();

            _peopleService
                .Setup(x => x.GetPercentagePeopleWithSameNameByRegion(region, name))
                .ReturnsAsync(expectedResult);

            var response = await _peopleController
                .GetPercentagePeopleWithSameNameByRegion(region, name);

            var result = response.Result as OkObjectResult;
            result.Value.Should().Be(expectedResult);
            result.StatusCode.Should().Be(StatusCodes.Status200OK);
        }

        [Fact]
        public async Task GetPercentagePeopleWithSameNameByRegionShouldToReturnBadRequest()
        {
            var name = _fixture.Create<string>();
            var region = _fixture.Create<Region>();
            var expectedResult = _fixture.Create<decimal>();

            _peopleService
                .Setup(x => x.GetPercentagePeopleWithSameNameByRegion(region, name))
                .ThrowsAsync(new Exception());

            var response = await _peopleController
                .GetPercentagePeopleWithSameNameByRegion(region, name);

            var result = response.Result as BadRequestObjectResult;
            result.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }

        [Fact]
        public async Task GetFamilyTreeByLevelShouldToReturnOk()
        {
            var level = _fixture.Create<int>();
            var name = _fixture.Create<string>();
            var expectedResult = _fixture.CreateMany<Person>();

            _peopleService
                .Setup(x => x.GetFamilyTreeByLevel(level, name))
                .ReturnsAsync(expectedResult);

            var response = await _peopleController.GetFamilyTreeByLevel(level, name);

            var result = response.Result as OkObjectResult;
            result.Value.Should().Be(expectedResult);
            result.StatusCode.Should().Be(StatusCodes.Status200OK);
        }

        [Fact]
        public async Task GetFamilyTreeByLevelShouldBeToReturnBadRequest()
        {
            var level = _fixture.Create<int>();
            var name = _fixture.Create<string>();
            var expectedResult = _fixture.CreateMany<Person>();

            _peopleService
                .Setup(x => x.GetFamilyTreeByLevel(level, name))
                .ThrowsAsync(new Exception());

            var response = await _peopleController.GetFamilyTreeByLevel(level, name);

            var result = response.Result as BadRequestObjectResult;
            result.StatusCode.Should().Be(StatusCodes.Status400BadRequest);
        }


    }
}
