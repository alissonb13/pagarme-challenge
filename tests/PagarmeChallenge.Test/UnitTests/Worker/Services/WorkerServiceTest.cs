﻿using AutoFixture;
using FluentAssertions;
using Moq;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.RabbitMQ.Contants;
using PagarmeChallenge.Shared.RabbitMQ.Interfaces;
using PagarmeChallenge.Worker.Data.Repositories.Interfaces;
using PagarmeChallenge.Worker.Services;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace PagarmeChallenge.Test.UnitTests.Worker.Services
{
    public class WorkerServiceTest
    {
        private readonly Fixture _fixture;

        private readonly Mock<IConsumerService> _consumerService;
        private readonly Mock<IPersonRepository> _personRepository;

        private WorkerService _workerService;

        public WorkerServiceTest()
        {
            _fixture = new Fixture();

            _consumerService = new Mock<IConsumerService>();
            _personRepository = new Mock<IPersonRepository>();

            _workerService = new WorkerService(_consumerService.Object, _personRepository.Object);
        }

        [Fact]
        public async Task ExecuteAsyncShouldBeExecutedCorrectly()
        {
            var person = _fixture.Create<Person>();
            var cancellationToken = new CancellationToken();
            _consumerService
                .Setup(x => x.Consume(QueueNamesConstants.DemographicPersonalDataQueue, It.IsAny<Action<Person>>()))
                .Callback<string, Action<Person>>((queueName, actionPerson) =>
                {
                    queueName.Should().Be(QueueNamesConstants.DemographicPersonalDataQueue);
                    actionPerson.Invoke(person);
                });

            await _workerService
                .StartAsync(cancellationToken)
                .ConfigureAwait(false);
        }
    }
}
