# Pagarme Challenge
---
PagarmeChallenge is a solution designed to include data about a person and can then extract demographic information from that data. 


## Technologies
---
- Services: .NET Core 3.1
- Message Broker: RabbitMQ
- Database: MongoDB
- Containerization Platform: Docker


## Running Solution
---
To execute the solution, just clone the repository and enter the `pagarme-challenge` directory and then, at the terminal, enter the command `docker-compose up`.

## Projects
---

### Command Api
---
This service exposes an endpoint for registering a person's information in the JSON format represented below:
```
{
  "name": "string",
  "color": "Black",
  "region": "North",
  "parentsNames": [
    "string"
  ],
  "childrensNames": [
    "string"
  ],
  "educationalLevel": "Fundamental"
}
```

_Swagger: localhost:8001/swagger_


### Query Api
---
This service provides three endpoints for obtaining information about people registered according to the following urls:
- Filter: `http://localhost:8002/api/people/filter-people-by-parameters?Name=William&Color=Black&Region=North&EducationalLevel=Fundamental`

- Percentage: `http://localhost:8002/api/people/get-percentage-people-with-same-name-by-region?region=North&name=William`

_Swagger: localhost:8002/swagger_

