﻿using Microsoft.Extensions.Hosting;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.RabbitMQ.Contants;
using PagarmeChallenge.Shared.RabbitMQ.Interfaces;
using PagarmeChallenge.Worker.Data.Repositories.Interfaces;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PagarmeChallenge.Worker.Services
{
    public class WorkerService : BackgroundService
    {
        private readonly IConsumerService _consumerService;
        private readonly IPersonRepository _personRepository;

        public WorkerService(
            IConsumerService consumerService,
            IPersonRepository personRepository)
        {
            _consumerService = consumerService;
            _personRepository = personRepository;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _consumerService
                    .Consume<Person>(QueueNamesConstants.DemographicPersonalDataQueue,
                    ((person) =>
                    {
                        Console.WriteLine($"Inserting: {person.Name}");
                        _personRepository.Insert(person);
                    }));

                await Task.Delay(180000, stoppingToken);
            }
        }
    }
}
