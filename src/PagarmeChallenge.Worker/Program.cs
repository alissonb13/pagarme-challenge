﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using PagarmeChallenge.Shared.RabbitMQ.Extensions;
using PagarmeChallenge.Worker.Data;
using PagarmeChallenge.Worker.Data.Repositories;
using PagarmeChallenge.Worker.Data.Repositories.Interfaces;
using PagarmeChallenge.Worker.Data.Settings;
using PagarmeChallenge.Worker.Services;

namespace PagarmeChallenge.Worker
{
    public class Program
    {
        public static void Main(string[] args)
            => Host.CreateDefaultBuilder(args)
                .ConfigureServices((context, services) =>
                {
                    services.Configure<ChallengeDatabaseSettings>(
                        context.Configuration.GetSection(nameof(ChallengeDatabaseSettings)));

                    services.AddSingleton<IChallengeDatabaseSettings>(provider 
                        => provider.GetRequiredService<IOptions<ChallengeDatabaseSettings>>().Value);

                    services.AddRabbitMQConfigurations(context.Configuration)
                        .AddTransient<IChallengeContext, ChallengeContext>()
                        .AddScoped<IPersonRepository, PersonRepository>()
                        .AddHostedService<WorkerService>();
                })
                .Build()
                .Run();
    }
}