﻿using MongoDB.Driver;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Worker.Data.Settings;

namespace PagarmeChallenge.Worker.Data
{
    public class ChallengeContext : IChallengeContext
    {
        public ChallengeContext(IChallengeDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            People = database.GetCollection<Person>(settings.CollectionName);
        }

        public IMongoCollection<Person> People { get; }
    }
}
