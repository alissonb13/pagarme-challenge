﻿using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Worker.Data.Repositories.Interfaces;

namespace PagarmeChallenge.Worker.Data.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly IChallengeContext _context;

        public PersonRepository(IChallengeContext context)
            => _context = context;

        public void Insert(Person person)
            => _context.People.InsertOne(person);
    }
}
