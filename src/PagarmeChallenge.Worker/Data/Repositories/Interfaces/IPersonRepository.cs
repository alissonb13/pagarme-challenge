﻿using PagarmeChallenge.Shared.Domain.Entities;

namespace PagarmeChallenge.Worker.Data.Repositories.Interfaces
{
    public interface IPersonRepository
    {
        void Insert(Person person);
    }
}
