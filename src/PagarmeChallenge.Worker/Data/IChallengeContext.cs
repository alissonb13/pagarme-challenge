﻿using MongoDB.Driver;
using PagarmeChallenge.Shared.Domain.Entities;

namespace PagarmeChallenge.Worker.Data
{
    public interface IChallengeContext
    {
        IMongoCollection<Person> People { get; }
    }
}
