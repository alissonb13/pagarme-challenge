﻿using LinqKit;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using System;
using System.Linq.Expressions;

namespace PagarmeChallenge.Query.Api.Data.Models
{
    public class QueryParameters
    {
        public string Name { get; set; }
        public Color? Color { get; set; }
        public Region? Region { get; set; }
        public EducationalLevel? EducationalLevel { get; set; }

        public Expression<Func<Person, bool>> GetParameters()
        {
            Expression<Func<Person, bool>> expression = t => true;

            if (!string.IsNullOrEmpty(Name))
                expression = expression.And(t => t.Name.ToLower().Contains(Name.ToLower()));

            if(Color != null)
                expression = expression.And(t => t.Color.Equals(Color));

            if (Region != null)
                expression = expression.And(t => t.Region.Equals(Region));

            if (EducationalLevel != null)
                expression = expression.And(t => t.EducationalLevel.Equals(EducationalLevel));

            return expression;
        }
    }
}
