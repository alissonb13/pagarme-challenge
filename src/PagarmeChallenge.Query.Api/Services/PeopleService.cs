﻿using PagarmeChallenge.Query.Api.Data.Models;
using PagarmeChallenge.Query.Api.Interfaces;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PagarmeChallenge.Query.Api.Services
{
    public class PeopleService : IPeopleService
    {
        private readonly IPeopleRepository _peopleRepository;

        public PeopleService(IPeopleRepository peopleRepository)
            => _peopleRepository = peopleRepository;

        public async Task<IEnumerable<Person>> FilterByParameters(QueryParameters query) 
            => await _peopleRepository.FilterByParameters(query.GetParameters());

        public Task<IEnumerable<Person>> GetFamilyTreeByLevel(int level, string name)
        {
            throw new NotImplementedException();
        }

        public async Task<decimal> GetPercentagePeopleWithSameNameByRegion(Region region, string name)
        {
            var peopleByRegion = await _peopleRepository.GetPeopleByRegion(region);

            if (peopleByRegion.Count == 0)
                throw new Exception("There are no people in that region with that name.");

            var peopleWithSameName = peopleByRegion
                .FindAll(x => x.Name.ToLower().Contains(name.ToLower()));

            var percentage = (decimal)peopleWithSameName.Count / peopleByRegion.Count * 100;

            return decimal.Round(percentage, 2);
        }
    }
}
