using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using PagarmeChallenge.Query.Api.Data;
using PagarmeChallenge.Query.Api.Data.Repositories;
using PagarmeChallenge.Query.Api.Data.Settings;
using PagarmeChallenge.Query.Api.Interfaces;
using PagarmeChallenge.Query.Api.Services;
using System.Text.Json.Serialization;

namespace PagarmeChallenge.Query.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c
                => c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "PagarmeChallenge.Query.Api",
                    Version = "v1"
                }));

            services.Configure<ChallengeDatabaseSettings>(
                        Configuration.GetSection(nameof(ChallengeDatabaseSettings)));

            services.AddSingleton<IChallengeDatabaseSettings>(provider
                => provider.GetRequiredService<IOptions<ChallengeDatabaseSettings>>().Value);

            services.AddTransient<IChallengeContext, ChallengeContext>();
            services.AddScoped<IPeopleRepository, PeopleRepository>();
            services.AddScoped<IPeopleService, PeopleService>();

            services.AddControllers()
                    .AddJsonOptions(options
                        => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter())); ;
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Catalog API V1");
            });
        }
    }
}
