﻿using Microsoft.AspNetCore.Mvc;
using PagarmeChallenge.Query.Api.Data.Models;
using PagarmeChallenge.Query.Api.Interfaces;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PagarmeChallenge.Query.Api.Controllers
{
    [ApiController]
    [Route("api/people")]
    public class PeopleController : ControllerBase
    {
        private readonly IPeopleService _personService;

        public PeopleController(IPeopleService personService)
            => _personService = personService;

        [HttpGet("filter-people-by-parameters")]
        public async Task<ActionResult<IEnumerable<Person>>> FilterPeopleByParameters([FromQuery] QueryParameters query)
        {
            try
            {
                return Ok(await _personService.FilterByParameters(query));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-family-tree-by-level")]
        public async Task<ActionResult<IEnumerable<Person>>> GetFamilyTreeByLevel(int level, string name)
        {
            try
            {
                return Ok(await _personService.GetFamilyTreeByLevel(level, name));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("get-percentage-people-with-same-name-by-region")]
        public async Task<ActionResult<IEnumerable<Person>>> GetPercentagePeopleWithSameNameByRegion(Region region, string name)
        {
            try
            {
                return Ok(await _personService.GetPercentagePeopleWithSameNameByRegion(region, name));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
