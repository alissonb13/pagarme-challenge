﻿using PagarmeChallenge.Query.Api.Data.Models;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PagarmeChallenge.Query.Api.Interfaces
{
    public interface IPeopleService
    {
        Task<IEnumerable<Person>> FilterByParameters(QueryParameters query);
        Task<IEnumerable<Person>> GetFamilyTreeByLevel(int level, string name);
        Task<decimal> GetPercentagePeopleWithSameNameByRegion(Region region, string name);
    }
}
