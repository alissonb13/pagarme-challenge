﻿using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PagarmeChallenge.Query.Api.Interfaces
{
    public interface IPeopleRepository
    {
        Task<List<Person>> GetPeopleByName(string name);
        Task<List<Person>> GetPeopleByRegion(Region region);
        Task<List<Person>> FilterByParameters(Expression<Func<Person, bool>> expression);
    }
}
