﻿
namespace PagarmeChallenge.Query.Api.Data.Settings
{

    public class ChallengeDatabaseSettings : IChallengeDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
        public string CollectionName { get; set; }
    }
}
