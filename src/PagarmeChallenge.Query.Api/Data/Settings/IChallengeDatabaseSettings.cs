﻿namespace PagarmeChallenge.Query.Api.Data.Settings
{
    public interface IChallengeDatabaseSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
        string CollectionName { get; set; }
    }
}
