﻿using MongoDB.Driver;
using PagarmeChallenge.Shared.Domain.Entities;

namespace PagarmeChallenge.Query.Api.Data
{
    public interface IChallengeContext
    {
        IMongoCollection<Person> People { get; }
    }
}
