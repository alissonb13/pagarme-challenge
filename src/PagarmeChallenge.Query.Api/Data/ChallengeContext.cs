﻿using MongoDB.Driver;
using PagarmeChallenge.Query.Api.Data.Settings;
using PagarmeChallenge.Query.Api.Interfaces;
using PagarmeChallenge.Shared.Domain.Entities;

namespace PagarmeChallenge.Query.Api.Data
{
    public class ChallengeContext : IChallengeContext
    {
        public ChallengeContext(IChallengeDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            People = database.GetCollection<Person>(settings.CollectionName);
        }

        public IMongoCollection<Person> People { get; }
    }
}
