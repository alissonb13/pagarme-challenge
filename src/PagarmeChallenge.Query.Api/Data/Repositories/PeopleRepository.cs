﻿using MongoDB.Driver;
using PagarmeChallenge.Query.Api.Interfaces;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace PagarmeChallenge.Query.Api.Data.Repositories
{
    public class PeopleRepository : IPeopleRepository
    {
        private readonly IChallengeContext _context;

        public PeopleRepository(IChallengeContext context)
            => _context = context;

        public async Task<List<Person>> GetPeopleByName(string name)
            => await _context.People.Find(x => x.Name.Equals(name)).ToListAsync();

        public async Task<List<Person>> GetPeopleByRegion(Region region)
            => await _context.People.Find(x => x.Region.Equals(region)).ToListAsync();

        public async Task<List<Person>> FilterByParameters(Expression<Func<Person, bool>> expression)
            => await _context.People.Find(expression).ToListAsync();
    }
}
