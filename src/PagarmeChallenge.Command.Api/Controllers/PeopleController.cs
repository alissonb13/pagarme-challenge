﻿using Microsoft.AspNetCore.Mvc;
using PagarmeChallenge.Shared.Domain.Entities;
using PagarmeChallenge.Shared.RabbitMQ.Contants;
using PagarmeChallenge.Shared.RabbitMQ.Interfaces;
using System;
using System.Net;

namespace PagarmeChallenge.Command.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PeopleController : ControllerBase
    {
        private readonly IProducerService _producerService;

        public PeopleController(IProducerService producerService)
            => _producerService = producerService;

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.Accepted)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public IActionResult Insert([FromBody] Person person)
        {
            try
            {
                _producerService.Publish(QueueNamesConstants.DemographicPersonalDataQueue, person);
            }
            catch (Exception ex) 
            { 
                return BadRequest(ex.Message); 
            }

            return Accepted(new { Message = "Registration Successful!" });
        }
    }
}
