﻿using System.Collections.Generic;

namespace PagarmeChallenge.Command.Api.Extensions
{
    public static class StringExtension
    {
        public static string ToPipedMessage(this IEnumerable<string> words)
            => words != null
            ? string.Join('|', words)
            : string.Empty;
    }
}
