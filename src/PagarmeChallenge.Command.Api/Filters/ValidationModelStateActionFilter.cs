﻿using Microsoft.AspNetCore.Mvc.Filters;
using PagarmeChallenge.Command.Api.Extensions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PagarmeChallenge.Command.Api.Filters
{
    public class ValidationModelStateActionFilter : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(
            ActionExecutingContext context, 
            ActionExecutionDelegate next)
        {
            var modelState = context.ModelState;

            if (!modelState.IsValid)
            {
                var errors = modelState
                    .Values
                    .SelectMany(value => value.Errors)
                    .Select(value => value.ErrorMessage)
                    .ToPipedMessage();

                throw new Exception(errors);
            }

            await next();
        }
    }
}
