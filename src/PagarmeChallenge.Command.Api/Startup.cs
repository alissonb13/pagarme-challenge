using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using PagarmeChallenge.Command.Api.Filters;
using PagarmeChallenge.Shared.RabbitMQ.Extensions;
using System.Text.Json.Serialization;

namespace PagarmeChallenge.Command.Api
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
            => Configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(c
                => c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "PagarmeChallenge.Command.Api",
                    Version = "v1"
                }));

            services.AddMvc(options
                => options.Filters.Add(typeof(ValidationModelStateActionFilter)));

            services.AddRabbitMQConfigurations(Configuration);

            services.AddControllers()
                    .AddFluentValidation(config
                        => config.RegisterValidatorsFromAssemblyContaining<Startup>())
                    .AddJsonOptions(options
                        => options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

            services.Configure<ApiBehaviorOptions>(options => options.SuppressModelStateInvalidFilter = true);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "PagarmeChallenge.Command.Api");
            });
        }
    }
}
