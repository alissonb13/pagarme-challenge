﻿namespace PagarmeChallenge.Shared.Domain.Entities.Enums
{
    public enum Color
    {
        Black,
        Brown,
        White,
        Yellow,
        Indigenous
    }
}
