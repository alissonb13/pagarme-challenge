﻿namespace PagarmeChallenge.Shared.Domain.Entities.Enums
{
    public enum Region
    {
        North,
        South,
        West,
        East,
        Northeast,
        Northwest,
        Southeast,
        Southwest
    }
}
