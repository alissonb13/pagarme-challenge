﻿namespace PagarmeChallenge.Shared.Domain.Entities.Enums
{
    public enum EducationalLevel
    {
        Fundamental,
        Medium,
        Superior,
        Postgraduate,
    }
}
