﻿using MongoDB.Bson.Serialization.Attributes;
using PagarmeChallenge.Shared.Domain.Entities.Enums;
using System.Collections.Generic;

namespace PagarmeChallenge.Shared.Domain.Entities
{
    public class Person
    {
        [BsonId]
        public string Name { get; set; }
        public Color Color { get; set; }
        public Region Region { get; set; }
        public List<string> ParentsNames { get; set; }
        public List<string> ChildrensNames { get; set; }
        public EducationalLevel EducationalLevel { get; set; }
    }
}
