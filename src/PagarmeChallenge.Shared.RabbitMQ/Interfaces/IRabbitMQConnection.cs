﻿using RabbitMQ.Client;
using System;

namespace PagarmeChallenge.Shared.RabbitMQ.Interfaces
{
    public interface IRabbitMQConnection : IDisposable
    {
        bool IsConnected { get; }

        bool TryConnect();

        IModel CreateModel();
    }
}
