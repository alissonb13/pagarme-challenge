﻿namespace PagarmeChallenge.Shared.RabbitMQ.Interfaces
{
    public interface IProducerService
    {
        void Publish(string queueName, object publishModel);
    }
}
