﻿using System;

namespace PagarmeChallenge.Shared.RabbitMQ.Interfaces
{
    public interface IConsumerService
    {
        void Consume<T>(string queueName, Action<T> action);
    }
}
