﻿using PagarmeChallenge.Shared.RabbitMQ.Extensions;
using PagarmeChallenge.Shared.RabbitMQ.Interfaces;
using RabbitMQ.Client.Events;
using System;

namespace PagarmeChallenge.Shared.RabbitMQ.Services
{
    public class ConsumerService : IConsumerService
    {
        private readonly IRabbitMQConnection _rabbitMQConnection;

        public ConsumerService(IRabbitMQConnection rabbitMQConnection)
            => _rabbitMQConnection = rabbitMQConnection;

        public void Consume<T>(string queueName, Action<T> callback)
        {
            var channel = _rabbitMQConnection.CreateModel();

            channel.QueueDeclare(
                queue: queueName,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var consumer = new EventingBasicConsumer(channel);

            consumer.Received += (model, eventArgs) =>
            {
                try
                {
                    var body = eventArgs.Body.ToArray();
                    var message = body.DeserializeBody<T>();

                    callback(message);

                    channel.BasicAck(eventArgs.DeliveryTag, false);
                }
                catch (Exception)
                {
                    channel.BasicNack(eventArgs.DeliveryTag, false, true);
                }
            };

            channel.BasicConsume(
                queue: queueName,
                autoAck: false,
                consumerTag: "",
                noLocal: false,
                exclusive: false,
                arguments: null,
                consumer: consumer);
        }
    }
}
