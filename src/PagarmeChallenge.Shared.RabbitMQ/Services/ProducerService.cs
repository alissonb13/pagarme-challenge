﻿using Newtonsoft.Json;
using PagarmeChallenge.Shared.RabbitMQ.Interfaces;
using RabbitMQ.Client;
using System;
using System.Text;

namespace PagarmeChallenge.Shared.RabbitMQ.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IRabbitMQConnection _rabbitMQConnection;

        public ProducerService(IRabbitMQConnection rabbitMQConnection)
            => _rabbitMQConnection = rabbitMQConnection;

        public void Publish(string queueName, object publishModel)
        {
            using var channel = _rabbitMQConnection.CreateModel();

            channel.QueueDeclare(
                queue: queueName,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var message = JsonConvert.SerializeObject(publishModel);
            var body = Encoding.UTF8.GetBytes(message);

            IBasicProperties properties = channel.CreateBasicProperties();
            properties.Persistent = true;
            properties.DeliveryMode = 2;

            channel.ConfirmSelect();

            channel.BasicPublish(
                exchange: "",
                routingKey: queueName,
                mandatory: true,
                basicProperties: properties,
                body: body);

            channel.WaitForConfirmsOrDie();

            channel.BasicAcks += (sender, eventArgs) => Console.WriteLine("Sent RabbitMQ");

            channel.ConfirmSelect();
        }
    }
}
