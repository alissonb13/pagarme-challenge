﻿namespace PagarmeChallenge.Shared.RabbitMQ.Contants
{
    public static class QueueNamesConstants
    {
        public const string DemographicPersonalDataQueue = "demographicPersonalDataQueue";
    }
}
