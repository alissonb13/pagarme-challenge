﻿using PagarmeChallenge.Shared.RabbitMQ.Interfaces;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;
using System;
using System.Threading;

namespace PagarmeChallenge.Shared.RabbitMQ.Connection
{
    public class RabbitMQConnection : IRabbitMQConnection
    {
        private IConnection _connection;
        private readonly IConnectionFactory _connectionFactory;

        public bool IsConnected
        {
            get => _connection != null && _connection.IsOpen;
        }

        public RabbitMQConnection(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;

            if (!IsConnected) TryConnect();
        }

        public IModel CreateModel()
        {
            if (!IsConnected)
                throw new InvalidOperationException("No connection with RabbitMQ");

            return _connection.CreateModel();
        }

        public bool TryConnect()
        {
            try
            {
                _connection = _connectionFactory.CreateConnection();
            }
            catch (BrokerUnreachableException)
            {
                Thread.Sleep(2000);
                _connection = _connectionFactory.CreateConnection();
                throw;
            }

            return IsConnected;
        }

        public void Dispose() => _connection.Dispose();
    }
}
