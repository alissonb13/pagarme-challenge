﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PagarmeChallenge.Shared.RabbitMQ.Connection;
using PagarmeChallenge.Shared.RabbitMQ.Interfaces;
using PagarmeChallenge.Shared.RabbitMQ.Services;
using RabbitMQ.Client;

namespace PagarmeChallenge.Shared.RabbitMQ.Extensions
{
    public static class RabbitMQConfigExtension
    {
        public static IServiceCollection AddRabbitMQConfigurations(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IRabbitMQConnection>(x =>
            {
                var factory = new ConnectionFactory()
                {
                    HostName = "rabbitmq",
                    UserName = "admin",
                    Password = "secret",
                    Port = 5672
                };

                return new RabbitMQConnection(factory);
            });

            services.AddSingleton<IProducerService, ProducerService>();
            services.AddSingleton<IConsumerService, ConsumerService>();

            return services;
        }
    }
}
