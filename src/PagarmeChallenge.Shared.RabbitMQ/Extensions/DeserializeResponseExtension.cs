﻿using Newtonsoft.Json;
using System.Text;

namespace PagarmeChallenge.Shared.RabbitMQ.Extensions
{
    public static class DeserializeResponseExtension
    {
        internal static T DeserializeBody<T>(this byte[] response)
        {
            var responseBody = Encoding.UTF8.GetString(response);
            return JsonConvert.DeserializeObject<T>(responseBody);
        }
    }
}
